# Star Wars Api Test Module

## About

A challenge for current, and potential idfive Backend developers, to assess
knowledge and skills in Drupal custom module creation, as well as working with
third party API's, and coding, formatting, and documenting to standard. Use
both this documentation site of idfive preferences, and examples, as well as
any relevant Drupal documentation desired to enact this module

- For a full description of idfive endpoint module challenge see [developers.idfive.com](https://developers.idfive.com/#/challenges/drupal-api-module)

### Module functionality requirements

The challenge was to create a custom Drupal 8 module that accomplishes the following:

- Interacts with The Star Wars API, to show a listing of 3 Star Wars films from `https://swapi.co/api/films` in any order you desire.
- These films should display in a block, that could be enabled on any region in a stock or custom theme.
- Each individual film view should include: title, episode_id, director, producer, and release_date fields at a minimum.
- release_date field to be formatted in F j, Y format (ie "January 1, 1977") on frontend.
- Conforms to current Drupal coding standards.
- Includes a Unit test for all custom functions.
- Includes all needed documentation per Drupal and idfive standards.
- Includes a composer.json, as if ready to deploy to packagist.
- Passes WAVE AA level testing.
- Passes W3C Validator testing.

## Installation

Install as you would normally install a contributed Drupal module. Visit [drupal.org](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules) for further information.

## Configuration

- Add the created block to the desired region in your theme.

## Options

- Module provides no configurable options.

## Development

- No special development considerations.
- See [developers.idfive.com](https://developers.idfive.com/), and [drupal documentation standards](https://www.drupal.org/docs/develop/documenting-your-project/module-documentation-guidelines).
