<?php

namespace Drupal\Tests\star_wars_api_test\StarWarsClientServiceTest;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use Drupal\star_wars_api_test\StarWarsClientService;
use GuzzleHttp\Client;

/**
 * Tests basic custom PHP functions.
 *
 * @group star_wars_api_test
 */
class StarWarsClientServiceTest extends UnitTestCase {

  /**
   * Before a test method is run, setUp() is invoked.
   *
   * Create new starwarsclientservice object.
   */
  public function setUp() {
    $container = new ContainerBuilder();
    \Drupal::setContainer($container);
    $client = new Client();
    $this->starwarsclientservice = new StarWarsClientService($client);
  }

  /**
   * @covers Drupal\star_wars_api_test\StarWarsClientService::getMovies
   *
   * Tests that 4 items are returned in json from the service, when we request 4.
   */
  public function testGetMovies() {
    $this->assertEquals(4, count($this->starwarsclientservice->getMovies(4)));
  }

  /**
   * Once test method has finished running, tearDown() will be invoked.
   *
   * Unset the $starwarsclientservice object.
   */
  public function tearDown() {
    unset($this->starwarsclientservice);
  }

}
