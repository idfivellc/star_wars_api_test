<?php

namespace Drupal\star_wars_api_test\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'StarWarsBlock' block.
 *
 * @Block(
 *  id = "star_wars_block",
 *  admin_label = @Translation("Star Wars block"),
 * )
 */
class StarWarsBlock extends BlockBase
{

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $config = \Drupal::config('star_wars_api_test.settings');
    $num = $config->get('films') ?: 0;

    $client = \Drupal::service('star_wars_api_test.client');

    $movies = $client->getMovies($num);
    $build = [];
    $build['#cache'] = [
      'max-age' => 0,
    ];
    for ($i = 0; $i < count($movies); $i++) {
      $build['#films'][$i]['title'] = $movies[$i]['title'];
      $build['#films'][$i]['episode_id'] = $movies[$i]['episode_id'];
      $build['#films'][$i]['director'] = $movies[$i]['director'];
      $build['#films'][$i]['producer'] = $movies[$i]['producer'];
      $build['#films'][$i]['release_date'] = $movies[$i]['release_date'];
    }
    $build['#theme'] = 'star_wars_api_test';
    return $build;
  }
}
