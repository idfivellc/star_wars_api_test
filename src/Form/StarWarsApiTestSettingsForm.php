<?php

namespace Drupal\star_wars_api_test\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class StarWarsApiTestSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'star_wars_api_test.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'star_wars_api_test_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['films'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number'),
      '#description' => $this->t('Set number of movies to return'),
      '#default_value' => $config->get('films'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
            // Set the submitted configuration setting.
      ->set('films', $form_state->getValue('films'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
