<?php

namespace Drupal\star_wars_api_test;

use GuzzleHttp\Client;

/**
 * Class StarWarsClientService.
 */
class StarWarsClientService {

  /**
   * Borrows Drupal http client injected here.
   *
   * @var GuzzleHttp\Client
   */
  protected $client;

  /**
   * Constructs a new StarWarsClientService object.
   */
  public function __construct(Client $client) {
    $this->client = $client;
  }

  /**
   * Public method to return random set of movies.
   */
  public function getMovies($num) {
    $random_movies = [];
    $url = 'https://swapi.co/api/films/';
    $movies = json_decode($this->client->get($url)->getBody()->__toString(), TRUE);
    $results = $movies['results'];
    if ($num && $num <= count($results) && $num > 0) {
      $random_keys = array_rand($results, $num);
      foreach ($random_keys as $key) {
        array_push($random_movies, $results[$key]);
      }
    }
    else {
      $random_movies = $results;
    }
    return $random_movies;
  }

}
